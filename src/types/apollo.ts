import { ApolloClient } from '@apollo/client'

export type Client = ApolloClient<Record<string, any>>

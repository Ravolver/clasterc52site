export { default as ModalHeader } from './ModalHeader/ModalHeader'
export { default as ModalFooter } from './ModalFooter/ModalFooter'

export * from './ModalHeader/ModalHeader'
export * from './ModalFooter/ModalFooter'
